//
//  YVTableForm.swift
//  YVSwiftTableForms
//
//  Created by Yash on 09/05/16.
//  Copyright © 2016 Yash. All rights reserved.
//

import UIKit

class YVTableForm: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Mark: - Enum
    enum YVSectionTextAlignment {
        case Left, Right, Center
    }
    
    // Mark: - iVar or Properties
    
    var tblForm: UITableView? {
        didSet {
            assert(tblForm != nil, "Table View Can't be nil.")
            
            tblForm?.delegate = self
            tblForm?.dataSource = self
            
            tblForm?.registerNib(UINib.init(nibName: "YV"+YVRowType.TextOnlyInput.rawValue+"Cell", bundle: nil), forCellReuseIdentifier: YVRowType.TextOnlyInput.rawValue)
            tblForm?.registerNib(UINib.init(nibName: "YV"+YVRowType.LabelTextInput.rawValue+"Cell", bundle: nil), forCellReuseIdentifier: YVRowType.LabelTextInput.rawValue)
            tblForm?.registerNib(UINib.init(nibName: "YV"+YVRowType.Button.rawValue+"Cell", bundle: nil), forCellReuseIdentifier: YVRowType.Button.rawValue)
        }
    }
    
    
    var arrSections: [String?] = []
    var arrRows: [[YVTableRow?]?] = [[]]
    
    
    // MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Mark: - Methods
    
    func appendSection(sectionTitle: String?) {
        
        if arrSections.count == 0 && arrRows.count > 0 {
            let arrSection: [YVTableRow?]? = arrRows[0]
            if arrSection?.count > 0 {
                arrSections.append(nil)
            }
        }
        arrSections.append(sectionTitle)
    }
    
    func updateSection(sectionTitle: String, atIndex: Int) {
        
        if atIndex < arrSections.count {
            
            arrSections[atIndex] = sectionTitle
            
        } else {
            
            for i in 0...atIndex {
                
                if i >= arrSections.count {
                    if i == atIndex {
                        self.appendSection(sectionTitle)
                    } else {
                        self.appendSection(nil)
                    }
                }
            }
            
        }
        
    }
    
    func appendRow(row: YVTableRow, toSection: Int) {
        
        if arrSections.count == 0 {
            
            let arrSection: [YVTableRow?] = [row]
            arrRows.append(arrSection)
            
        } else if toSection < arrRows.count && arrRows[toSection] != nil {
            
            var arrSection: [YVTableRow?]? = arrRows[toSection]
            arrSection?.append(row)
            arrRows[toSection] = arrSection
            
        } else {
            for i in arrRows.count...toSection {
                if i < toSection {
                    arrRows.append(nil)
                } else { //if (i == toSection) {
                    arrRows.append([row])
                }
                if arrSections.count < arrRows.count {
                    self.appendSection(nil)
                }
            }
        }
        
    }
    
    
    // MARK: - Table View Delegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        let sections = arrSections.count > 0 ? arrSections.count : 1
        return sections
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrRows.count > 0 && section < arrRows.count {
            let arrSection: [YVTableRow?]? = arrRows[section]
            let rows = arrSection?.count > 0 ? arrSection!.count : 0
            return rows
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let arrSection: [YVTableRow?]? = arrRows[indexPath.section]
        let row: YVTableRow = (arrSection?[indexPath.row])!
        
        let cell: UITableViewCell
        switch row.type {
        case .TextOnlyInput:
            let textOnlyInputCell = tableView.dequeueReusableCellWithIdentifier(YVRowType.TextOnlyInput.rawValue) as! YVTextOnlyInputCell
            textOnlyInputCell.textField.placeholder = row.text
            cell = textOnlyInputCell
            break
        case .LabelTextInput:
            let textOnlyInputCell = tableView.dequeueReusableCellWithIdentifier(YVRowType.LabelTextInput.rawValue) as! YV
            textOnlyInputCell.textField.placeholder = row.text
            cell = textOnlyInputCell
            break
        case .Button:
            cell = tableView.dequeueReusableCellWithIdentifier(YVRowType.Button.rawValue)!
            break
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionTitle = arrSections.count > 0 && arrSections[section]?.characters.count > 0 ? arrSections[section] : nil
        return sectionTitle
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.contentView.backgroundColor = UIColor.redColor()
        headerView.textLabel?.textColor = UIColor.yellowColor()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
