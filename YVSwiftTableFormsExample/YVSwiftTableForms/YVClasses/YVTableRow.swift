//
//  YVTableRow.swift
//  YVSwiftTableForms
//
//  Created by Yash on 09/05/16.
//  Copyright © 2016 Yash. All rights reserved.
//

import UIKit

enum YVRowType: String {
    case TextOnlyInput, LabelTextInput, Button
}

class YVTableRow: NSObject {
    
    var type: YVRowType = YVRowType.TextOnlyInput
    var text: String?
    var value: String?
    var indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    override init() {
        super.init()
    }
}
